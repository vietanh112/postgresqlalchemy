# postgreSQLalchemy
## env
1. Ubuntu 18.04.6 LTS
2. python 3.9.15
3. postgreSQL 

## requirements
1. `pip install -r requirements.txt`
2. [install](https://www.postgresql.org/download/linux/ubuntu/) postgreSQL
3. [check version](https://phoenixnap.com/kb/check-postgresql-version) postgreSQL
4. [MySQL Workbench](https://phoenixnap.com/kb/mysql-workbench-ubuntu)
5. [Navicat Monitor](https://www.navicat.com/en/navicat-monitor-installation-guide?ver=ubuntu)
6. [DB Browser for SQLite](https://sqlitebrowser.org/dl/)
7. [pqAdmin 4](https://www.pgadmin.org/download/pgadmin-4-apt/)

## tutorial
1. sqlite3 & sqlalchemy [viblo 1](https://viblo.asia/p/gioi-thieu-sqlalchemy-trong-pythonpart-1-Q7eERE9rRgNj), [viblo 2](https://viblo.asia/p/gioi-thieu-sqlalchemy-trong-python-part-2-5y8Rr7ZxMob3#_sqlalchemy-5), [viblo 3](https://viblo.asia/p/sqlalchemy-trong-python-part-3-pVYRPjNVG4ng#_tong-quan-0), [viblo 4](https://viblo.asia/p/sqlalchemy-trong-python-part-4-157G5oY8RAje#_gioi-thieu-0)
2. sqlite without sqlalchemy [viblo](https://viblo.asia/p/sqlite-la-gi-E375zVVR5GW)
3. Databases with Python: Postgres, SQLAlchemy, and Alembic [learndatasci](https://www.learndatasci.com/tutorials/using-databases-python-postgres-sqlalchemy-and-alembic/)
4. 