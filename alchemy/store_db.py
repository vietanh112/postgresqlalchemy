from sqlalchemy.orm import relationship, declarative_base, sessionmaker
from sqlalchemy.types import String, Integer, DateTime, LargeBinary
from sqlalchemy.schema import Column, ForeignKey, MetaData
from sqlalchemy.engine import create_engine
from snowflake import Snowflake, SnowflakeGenerator
from time import time
from datetime import datetime
from nanoid import generate
from base64 import b64decode, b64encode
from typing import Callable, Union
import numpy as np
import pandas as pd
import polars as pl
import os, posixpath as P
import cv2
import warnings, logging
warnings.filterwarnings('error')

def create_logger(file):
    logger = logging.getLogger(file)
    logger.setLevel(level=logging.NOTSET)
    fileHandle = logging.FileHandler('app/weights/logging.log')
    fileHandle.setLevel(logging.NOTSET)
    formatter = logging.Formatter('%(asctime)s - %(name)s - L %(lineno)d - %(levelname)s - %(message)s')
    fileHandle.setFormatter(formatter)
    logger.addHandler(fileHandle)
    return logger

logger = create_logger(__file__); del create_logger


def encode(image:cv2.Mat) -> bytes:
    retval, buffer = cv2.imencode('.jpg', image)
    return b64encode(buffer)

def decode(encoded_str:bytes) -> cv2.Mat:
    im_bytes = b64decode(encoded_str)
    im_arr = np.frombuffer(im_bytes, dtype=np.uint8)    # im_arr is one-dim Numpy array
    return cv2.imdecode(im_arr, flags=cv2.IMREAD_COLOR)

def vec2byte(x:np.ndarray) -> bytes:
    return x.astype(np.float32).tobytes()

def byte2vec(x:bytes) -> np.float32:
    return np.frombuffer(x, dtype=np.float32)


Base = declarative_base()
idx = dict(index=True, unique=True, primary_key=True)

class Person(Base):
    __tablename__ = '__person__'
    id   = Column(Integer, **idx)
    name = Column(String(64))
    data = relationship("Data", back_populates="person")
    time = relationship("TimeCheckin", back_populates="person")
    
    description = Column(String(512), nullable=True, default='')
    created_by  = Column(String(64), nullable=True, default='')
    updated_by  = Column(String(64), nullable=True, default='')
    created_at  = Column(DateTime, default=datetime.utcnow())
    updated_at  = Column(DateTime, default=datetime.utcnow())


class Data(Base):
    __tablename__ = '__data__'
    id       = Column(Integer, autoincrement='auto', **idx)
    imgb64   = Column(LargeBinary)
    feat     = Column(LargeBinary)
    imgname  = Column(String, unique=True, default=generate())
    
    person_id = Column(Integer, ForeignKey(Person.id))
    person = relationship("Person", back_populates="data")
    
    description = Column(String(512), nullable=True, default='')
    created_by  = Column(String(64), nullable=True, default='')
    updated_by  = Column(String(64), nullable=True, default='')
    created_at  = Column(DateTime, default=datetime.utcnow())
    updated_at  = Column(DateTime, default=datetime.utcnow())
    

class TimeCheckin(Base):
    __tablename__ = '__timecheckin__'
    id       = Column(Integer, autoincrement='auto', **idx)
    imgb64   = Column(LargeBinary)
    timestamp = Column(Integer, default=int(time() * 1E3))
    
    person_id = Column(Integer, ForeignKey(Person.id))
    person = relationship("Person", back_populates="time")

    description = Column(String(512), nullable=True, default='')
    created_by  = Column(String(64), nullable=True, default='')
    updated_by  = Column(String(64), nullable=True, default='')
    created_at  = Column(DateTime, default=datetime.utcnow())
    updated_at  = Column(DateTime, default=datetime.utcnow())


class CRUD(object):
    def __init__(self, db_path:str):
        # url = 'sqlite:///:memory:'  #in-memory sqlite
        self.url = "sqlite:///{}".format(db_path)
        # print(self.url)
        self.engine = create_engine(self.url, echo=False, future=True)
        person_tab = Person.__table__
        data_tab = Data.__table__
        time_tab = TimeCheckin.__table__
        meta = MetaData()
        meta.create_all(bind=self.engine, tables=[person_tab, data_tab, time_tab])
        self.Session = sessionmaker(bind=self.engine, autoflush=False)
    
    def get_all_person(self) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
        """
        lấy thông tin 3 feat [id, fname, feat x3]
        """
        with self.Session() as sess:
            stmt = sess.query(Person.id, Person.name, Data.feat)    \
                .filter(Person.id == Data.person_id).statement
                
        df = pl.read_sql(str(stmt.compile(self.engine)), self.url)
        # df = pd.read_sql(stmt, self.url)
        ids = df['id'].to_numpy()
        names = df['name'].to_numpy()
        feats = df['feat'].to_numpy()
        feats = np.array([byte2vec(ft) for ft in feats])
        
        return ids, names, feats
    
    def get_data_id(self, person_id:int) -> Union[list[int], list]:
        if self.check_id_exists(person_id=person_id):
            with self.Session() as sess:
                data_ids = sess.query(Data.id)\
                    .filter(Data.person_id == person_id).all()
                data_ids = [idx['id'] for idx in data_ids]
                return data_ids
        return []
    
    def update_feature_from_feat(self, data_id:int, new_feat:np.ndarray):
        with self.Session() as sess:
            sess.query(Data).filter(Data.id == data_id)   \
                .update({Data.feat       : vec2byte(new_feat),
                         Data.updated_at : datetime.utcnow()}, synchronize_session="fetch")
            sess.commit()

    def update_feature_from_extractor(self, data_id: int, extractor: Callable):
        with self.Session() as sess:
            data = sess.query(Data).filter(Data.id == data_id)
            img = data.with_entities(Data.imgb64).scalar()
            new_feat = extractor(decode(img))
            data.update({Data.feat       : vec2byte(new_feat),
                         Data.updated_at : datetime.utcnow()}, synchronize_session="fetch")
            sess.commit()

    def get_features(self, person_id:int) -> list[np.ndarray]:
        with self.Session() as sess:
            feats = sess.query(Data.feat).filter(Data.person_id == person_id).all()
            feats = [byte2vec(ft['feat']) for ft in feats]
            return feats
            
    def check_id_exists(self, person_id:int) -> bool:
        """
        kiểm tra xem Person.id đã tồn tại trong bảng chưa
        return `True`  if id đã     tồn tại
        return `False` if id không  tồn tại
        """
        with self.Session() as sess:
            res = sess.query(Person).filter_by(id=person_id).all()
            if len(res) > 0:
                logger.info('id {id:6s} belong to {name:s}'.format(
                    id=str(person_id).rjust(6, '0'), name=res[0].name))
                return True
            logger.info('id: {} không tồn tại'.format(person_id))
            return False
    
    def register_new_person(self, person_id:int, name:str, 
                            imgs:list[cv2.Mat], feats:list[np.ndarray]) -> bool:
        """
        return True nếu đăng ký người mới thành công
        return False nếu id đã tồn tại trong db, đăng ký ko thành công
        """
        assert len(imgs) == 5, "each people register by provide 3 image of face"
        assert len(imgs) == len(feats)
        
        if not self.check_id_exists(person_id):
            newP = Person(id=person_id, name=name)
            with self.Session() as sess:
                sess.add(newP)
                for i, img, feat in zip(range(1, len(imgs)+1), imgs, feats):
                    imgname = f'{newP.id}-{i}.jpg'
                    # cv2.imwrite(P.join(IMG_ROOT, imgname), img)
                    newD = Data(imgb64=encode(img), feat=vec2byte(feat),
                                person_id=newP.id, imgname=imgname)
                    sess.add(newD)
                sess.commit()
            return True
        return False

    def delete_old_person(self, person_id:int) -> bool:
        """
        return True nếu xóa     thành công
        return False nếu xóa k thành công, person_id ko tồn tại
        """
        if self.check_id_exists(person_id):
            with self.Session() as sess:
                sess.query(Person)                      \
                    .filter(Person.id == person_id)     \
                    .delete(synchronize_session="fetch")
                
                sess.query(Data)                        \
                    .filter(Data.person_id == person_id)\
                    .delete(synchronize_session="fetch")
                sess.commit()
            print('remove id {} susscess'.format(person_id))
            return True
        return False
    
    def record_time_checkin(self, person_id:int, img:cv2.Mat, timestamp:int=int(time() * 1e3)) -> bool:
        if self.check_id_exists(person_id):
            newT = TimeCheckin(person_id=person_id, imgb64=encode(img), timestamp=timestamp)
            with self.Session() as sess:
                sess.add(newT)
                sess.commit()
            return True
        return False
    
    
if __name__ == '__main__':
    from glob import iglob
    db = CRUD()

    def register_new_person():
        fold = 'data/14_11_face_n_feat/db'
        for i, sub in enumerate(os.scandir(fold), start=100):
            if sub.is_dir():
                imgs = []; feats = []
                for imgp in iglob(P.join(sub, '*.jpg')):
                    imgs.append(cv2.imread(imgp))
                    
                for fp in iglob(P.join(sub, '*.npy')):
                    feats.append(np.load(fp))
                    
                db.register_new_person(i, P.basename(sub.path), imgs, feats)

    def check_id_exists(id:int=200) -> bool:
        """
        kiểm tra xem id_person đã tồn tại trong table hay chưa
        """
        db.id_not_exists(id=id)

    def get_all_person():
        """
        lấy tất cả dữ liệu từ db để tạo pl.DataFrame
        
        """
        df:pl.DataFrame = db.get_all_person()
        print(type(df), df.shape)
        print(df.columns)
        print(df.head())

        person = df['id'].to_numpy()
        print(type(person))
        print(person)

    def get_features(id_person=100):
        db.get_features(id_person)
        # (512,) float32 -17.885206
        # (512,) float32 -20.64312
        # (512,) float32 -32.94043
    
    def delete_old_person(person_id=100):
        suss = db.delete_old_person(person_id)
        

    def update_feature_from_feat():
        def check_feat():
            feats = db.get_features(person_id=101)
            for ft in feats:
                print(type(ft), ft.shape, ft.sum())
            print()
            
        check_feat()
        db.update_feature_from_feat(4, np.full(512, fill_value=0.125, dtype=np.float32))
        check_feat()

    def update_feature_from_extractor():
        def check_feat():
            feats = db.get_features(person_id=101)
            for ft in feats:
                print(type(ft), ft.shape, ft.sum())
            print()
        
        extractor = lambda x: np.full(512, fill_value=0.5, dtype=np.float32)
        
        check_feat()
        db.update_feature_from_extractor(data_id=4, extractor=extractor)
        check_feat()
    
    # register_new_person()